export class EventBus<DetailType = any> {
  private eventTarget: EventTarget;

  public constructor(description = '') {
    this.eventTarget = document.appendChild(document.createComment(description));
  }

  public on(type: string, listener: (event: Event | CustomEvent<DetailType>) => void): void {
    this.eventTarget.addEventListener(type, listener);
  }

  public once(type: string, listener: (event: Event | CustomEvent<DetailType>) => void): void {
    this.eventTarget.addEventListener(type, listener, { once: true });
  }

  public off(type: string, listener: (event: Event | CustomEvent<DetailType>) => void): void {
    this.eventTarget.removeEventListener(type, listener);
  }

  public emit(type: string, detail?: DetailType): boolean {
    return this.eventTarget.dispatchEvent(new CustomEvent(type, { detail }));
  }
}

// Usage
// const myEventBus = new EventBus<string>('my-event-bus');
// myEventBus.on('event-name', ({ detail }) => {
//     document.body.append(detail + ' ');
// });
// myEventBus.once('event-name', ({ detail }) => {
//     document.body.append(detail + ' ');
// });
// myEventBus.emit('event-name', 'Hello'); // => Hello Hello
// myEventBus.emit('event-name', 'World'); // => World
