import { library } from '@fortawesome/fontawesome-svg-core';
import { fab, faFacebook, faGithubAlt, faGoogle, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faAddressCard } from '@fortawesome/free-regular-svg-icons';
import { faCode, faHighlighter, faImages, faUserGraduate } from '@fortawesome/free-solid-svg-icons';

library.add(faCode, faHighlighter, fab, faAddressCard, faUserGraduate, faImages, faGithubAlt, faGoogle, faFacebook, faTwitter);
