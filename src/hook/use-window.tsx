import { Dispatch, useState } from 'react';

import { useDocument } from './use-document';

type GlobalWindow = Window & typeof globalThis;

const useWindow = (): [GlobalWindow, Dispatch<GlobalWindow>] => {
  const [globalDocument] = useDocument();
  const [instanceWindow, setInstanceWindow] = useState<GlobalWindow>(globalDocument.defaultView);

  const setGlobalWindow = (value: GlobalWindow) => {
    setInstanceWindow(value);
  };

  return [instanceWindow, setGlobalWindow];
};

export { useWindow };
