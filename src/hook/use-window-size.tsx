import { useEffect, useState } from 'react';

import { useWindow } from './use-window';

const useWindowSize = () => {
  const [globalWindow] = useWindow();
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });
  useEffect(() => {
    const handleResize = () => {
      setWindowSize({
        width: globalWindow.innerWidth,
        height: globalWindow.innerHeight,
      });
    };
    globalWindow.addEventListener('resize', handleResize);
    handleResize();

    return () => globalWindow.removeEventListener('resize', handleResize);
  }, []);

  return windowSize;
};

export { useWindowSize };
