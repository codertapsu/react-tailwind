import { useState } from 'react';

import { useWindow } from './use-window';

const useLocalStorage = <T extends unknown>(key: string, initialValue?: T): [T, (value: T | ((val: T) => T)) => void] => {
  const [globalWindow] = useWindow();
  const [storage] = useState<Storage>(globalWindow.localStorage || new Storage());
  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      const item = storage.getItem(key);

      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      return initialValue;
    }
  });
  const setValue = (value: T | ((val: T) => T)) => {
    try {
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      setStoredValue(valueToStore);
      storage.setItem(key, JSON.stringify(valueToStore));
    } catch (error) {}
  };

  return [storedValue, setValue];
};

export default useLocalStorage;
