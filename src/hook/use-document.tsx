import { Dispatch, useState } from 'react';

const useDocument = (): [Document, Dispatch<Document>] => {
  const [instanceDocument, setInstanceDocument] = useState<Document>(document);

  const setDocument = (value: Document) => {
    setInstanceDocument(value);
  };

  return [instanceDocument, setDocument];
};

export { useDocument };
