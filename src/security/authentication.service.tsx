import { useHttpClient } from '@contexts/http-client/http-client.context';
import { Collection } from '@models/collection.model';

import { useAuthentication } from './authentication.context';
import { UserStatus } from './models/user-status.model';
import { UserDetails } from './models/user.model';

const useAuthenticationService = () => {
  const { setCurrentUser, setAuthToken } = useAuthentication();
  const httpClient = useHttpClient();

  const login = async (email: string, password: string): Promise<UserDetails> => {
    const response = await httpClient.post<{ employee: Collection<unknown>; token: string }>('auths', {
      password,
      login: email,
      connectionId: '',
    });

    const userDetailsFormatted: UserDetails = response?.employee && {
      userName: String(response.employee.userName),
      fullName: String(response.employee.fullName),
      userId: Number(response.employee.id),
      avatarUrl: '',
      badgeId: String(response.employee.badge),
      currentStatus: UserStatus.Active,
    };
    setCurrentUser(userDetailsFormatted);
    setAuthToken(response.token);

    return userDetailsFormatted;
  };

  return { login };
};

export { useAuthenticationService };
