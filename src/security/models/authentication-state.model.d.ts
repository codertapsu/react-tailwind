export interface AuthenticationState {
  authToken: string;
  setAuthToken?: (token: string) => void;
  currentUser: UserDetails;
  setCurrentUser?: (user: UserDetails) => void;
  updateCurrentUser?: (user: UserDetails) => void;
}
