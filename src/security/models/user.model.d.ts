import { UserStatus } from './user-status.model';

export interface UserDetails {
  userName: string;
  fullName: string;
  userId: number;
  avatarUrl: string;
  badgeId: string;
  currentStatus: UserStatus;
}
