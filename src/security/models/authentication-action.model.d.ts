export interface AuthenticationAction {
  type: 'SET_AUTH_TOKEN' | 'SET_USER_DETAILS' | 'UPDATE_USER_DETAILS';
  payload?: unknown;
}
