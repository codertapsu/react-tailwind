import { createContext, useContext, useMemo, useReducer } from 'react';

import useLocalStorage from '@hook/use-local-storage';

import { AuthenticationAction } from './models/authentication-action.model';
import { AuthenticationState } from './models/authentication-state.model';
import { UserDetails } from './models/user.model';

const AuthenticationContext = createContext<AuthenticationState>(undefined);

const useAuthentication = () => {
  const context = useContext(AuthenticationContext);
  if (context === undefined) {
    throw new Error('useAuthentication must be used within a AuthenticationProvider');
  }

  return {
    authToken: context.authToken,
    setAuthToken: context.setAuthToken,
    currentUser: context.currentUser,
    setCurrentUser: context.setCurrentUser,
    updateCurrentUser: context.updateCurrentUser,
  };
};

const authenticationReducer = (state: AuthenticationState, action: AuthenticationAction) => {
  switch (action.type) {
    case 'SET_AUTH_TOKEN':
      return { ...state, authToken: action.payload as string };
    case 'SET_USER_DETAILS':
      return { ...state, currentUser: action.payload as UserDetails };
    case 'UPDATE_USER_DETAILS':
      const currentUser = { ...state.currentUser, ...(action.payload as UserDetails) };

      return { ...state, currentUser };
    default:
      return state;
  }
};

const AuthenticationProvider = ({ children }: { children: unknown }) => {
  const [storedAuthToken, setStoredAuthToken] = useLocalStorage<string>('auth-token', '');
  const [_, setStoredUserId] = useLocalStorage<string>('user-id', '');
  const [state, dispatch] = useReducer(authenticationReducer, { authToken: storedAuthToken, currentUser: undefined });

  const setAuthToken = (token: string) => {
    setStoredAuthToken(token);
    dispatch({
      type: 'SET_AUTH_TOKEN',
      payload: token,
    });
  };
  const setCurrentUser = (user: UserDetails) => {
    setStoredUserId(user.badgeId);
    dispatch({
      type: 'SET_USER_DETAILS',
      payload: user,
    });
  };
  const updateCurrentUser = (user: Partial<UserDetails>) => {
    setStoredUserId(user.badgeId);
    dispatch({
      type: 'UPDATE_USER_DETAILS',
      payload: user,
    });
  };

  const memoedValue = useMemo(
    () => ({
      setAuthToken,
      setCurrentUser,
      updateCurrentUser,
      authToken: state.authToken,
      currentUser: state.currentUser,
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [state.currentUser],
  );

  return <AuthenticationContext.Provider value={memoedValue}>{children}</AuthenticationContext.Provider>;
};

export { AuthenticationProvider, useAuthentication };
