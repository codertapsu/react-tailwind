import ReactDOM from 'react-dom';

import App from './App';
import './assets/css/index.css';
import './assets/font-awesome';
import reportWebVitals from './reportWebVitals';
import './scss/styles.scss';

ReactDOM.render(<App />, document.getElementById('root'));
// window.addEventListener('DOMContentLoaded', () => {
//   ReactDOM.render(<App />, document.getElementById('root'));
// });

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
