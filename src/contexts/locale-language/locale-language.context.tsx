import { createContext, useContext, useEffect, useReducer } from 'react';
import { IntlProvider } from 'react-intl';

import { useWindow } from '@hook/use-window';

import languageMessage from './messages/en';

interface LocaleLanguageState {
  locale?: string;
  messages?: Record<string, string>;
  isDependOnSystem?: boolean;
  setLocaleLanguage?: (locale: string) => void;
}

const LocaleLanguageContext = createContext<LocaleLanguageState>(undefined);

const useLocaleLanguage = () => {
  const context = useContext(LocaleLanguageContext);
  if (context === undefined) {
    throw new Error('useLocaleLanguage must be used within a LocaleLanguageProvider');
  }

  return { setLocaleLanguage: context.setLocaleLanguage };
};

const localeLanguageReducer = (state: LocaleLanguageState, patch: Omit<LocaleLanguageState, 'setLocaleLanguage'>) => {
  return {
    ...state,
    locale: patch.locale ?? state.locale,
    messages: patch.messages ?? state.messages,
    isDependOnSystem: patch.isDependOnSystem ?? state.isDependOnSystem,
  };
};

const LocaleLanguageProvider = ({ children }: any) => {
  const [{ locale, messages, isDependOnSystem }, dispatch] = useReducer(localeLanguageReducer, {
    locale: 'en-US',
    messages: languageMessage,
    isDependOnSystem: false,
  });
  const [globalWindow] = useWindow();

  const setLocaleLanguage = (newLocale: string) => {
    if (newLocale !== locale) {
      let endpoint: string;

      const markDependOnSystem = newLocale === 'system';
      if (markDependOnSystem) {
        endpoint =
          (navigator.languages && navigator.languages[0]) ||
          navigator.language ||
          (navigator as unknown as Record<string, string>).userLanguage ||
          'en-US';
      } else {
        endpoint = newLocale.toLowerCase().substring(0, 2);
      }

      import(`./messages/${endpoint}`)
        .then(module => {
          dispatch({
            locale: newLocale,
            messages: module.default,
            isDependOnSystem: markDependOnSystem,
          });
        })
        .catch(() => {});
    }
  };

  useEffect(() => {
    globalWindow.addEventListener('languagechange', () => {
      if (isDependOnSystem) {
        const systemLanguage =
          (navigator.languages && navigator.languages[0]) ||
          navigator.language ||
          (navigator as unknown as Record<string, string>).userLanguage ||
          'en-US';
        setLocaleLanguage(systemLanguage);
      }
    });
  });

  const onError = (err: any) => {
    console.info(err.message);
  };

  return (
    <LocaleLanguageContext.Provider value={{ setLocaleLanguage }}>
      <IntlProvider locale={locale} messages={messages} onError={onError}>
        {children}
      </IntlProvider>
    </LocaleLanguageContext.Provider>
  );
};

export { LocaleLanguageProvider, useLocaleLanguage };
