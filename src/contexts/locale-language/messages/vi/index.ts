import { HomePageLang } from './home';

const compose = { ...HomePageLang };

export default compose;
