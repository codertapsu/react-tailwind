import axios from 'axios';
import { createContext, useContext, useEffect, useState } from 'react';

/* eslint-disable react-hooks/exhaustive-deps */
import { useAuthentication } from '@security/authentication.context';

import { BaseResponse } from '../../models/base-response.model';
import { Collection } from '../../models/collection.model';
import { HttpMethod } from './http-method.model';

const createQueryParam = (object: Collection): string => {
  if (!object) {
    return '';
  }

  const serialize = [];
  for (const key in object) {
    if (Object.prototype.hasOwnProperty.call(object, key)) {
      serialize.push(encodeURIComponent(key) + '=' + encodeURIComponent(String(object[key])));
    }
  }

  return serialize.join('&');
};

const handleAxiosError = (error: unknown) => {
  return Promise.reject(error);
};

const handleUnexpectedError = (error: unknown) => {
  return Promise.reject(error);
};

const isBaseResponse = (payload: any): payload is BaseResponse<unknown> => {
  return (
    typeof payload === 'object' &&
    Object.prototype.hasOwnProperty.call(payload, 'data') &&
    Object.prototype.hasOwnProperty.call(payload, 'failed') &&
    Object.prototype.hasOwnProperty.call(payload, 'succeeded')
  );
};

const axiosInstance = axios.create({
  baseURL: 'http://staging-api-eapproval.saigontechnology.vn',
});

const HttpClientContext = createContext<HttpMethod>(undefined);

const useHttpClient = () => {
  const context = useContext(HttpClientContext);
  if (context === undefined) {
    throw new Error('useHttpClient must be used within a HttpClientProvider');
  }

  return {
    get: context.get,
    post: context.post,
    put: context.put,
    delete: context.delete,
  };
};

const HttpClientProvider = ({ children }: any) => {
  const { authToken } = useAuthentication();
  const [http] = useState(() => {
    return axiosInstance;
  });

  useEffect(() => {
    http.interceptors.request.use(
      config => {
        config.headers.Authorization = `Bearer ${authToken}`;

        return config;
      },
      error => {
        return Promise.reject(error);
      },
    );
  }, [authToken]);

  useEffect(() => {
    http.interceptors.response.use(
      requestResponse => {
        const bodyResponse = requestResponse.data;
        if (bodyResponse && isBaseResponse(bodyResponse.data)) {
          if (bodyResponse.data.failed) {
            return Promise.reject({
              message: bodyResponse.data.message,
            });
          }

          return bodyResponse.data;
        }

        return bodyResponse?.data || bodyResponse;
      },
      error => {
        return axios.isAxiosError(error) ? handleAxiosError(error) : handleUnexpectedError(error);
      },
    );
  }, []);

  const get: HttpMethod['get'] = (path: string, query?: Collection) => {
    return http.get(path, { params: createQueryParam(query) });
  };
  const post: HttpMethod['post'] = (path: string, body: Collection, query?: Collection) => {
    return http.post(path, body, { params: createQueryParam(query) });
  };
  const put: HttpMethod['put'] = (path: string, body?: Collection, query?: Collection) => {
    return http.put(path, body, { params: createQueryParam(query) });
  };
  const deleteMethod: HttpMethod['delete'] = (path: string, query?: Collection) => {
    return http.delete(path, { params: createQueryParam(query) });
  };

  const value = {
    get,
    post,
    put,
    delete: deleteMethod,
  };

  return <HttpClientContext.Provider value={value}>{children}</HttpClientContext.Provider>;
};

export { HttpClientProvider, useHttpClient };
