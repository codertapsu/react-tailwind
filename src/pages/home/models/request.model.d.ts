export interface RequestShape {
  allowView: boolean;
  approve: {
    approveDate: string;
    approveName: string;
    approveStatus: number;
    comment: string;
  }[];
  allowViewAttachment: boolean;
  completedDate: string;
  documentTitle: string;
  id: number;
  isAccessRight: boolean;
  isNeedAttention: true;
  requestBy: string;
  requestDate: string;
  statusId: number;
  statusName: string;
  typeOfRequest: string;
}
