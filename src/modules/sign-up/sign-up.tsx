import React, { FormEvent } from 'react';
import { Link } from 'react-router-dom';

import styles from './sign-up.module.css';

// This is a uncontrolled form! No need to manage state for each input!
export default function SignUp() {
  async function handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();

    // const formData = new FormData(event.currentTarget);

    // signUp(
    //   formData.get("email") as string,
    //   formData.get("name") as string,
    //   formData.get("password") as string
    // );
  }

  return (
    <form className={styles.root} onSubmit={handleSubmit}>
      <h1>Sign up</h1>
      <label>
        Name
        <input name='name' />
      </label>

      <label>
        Email
        <input name='email' type='email' />
      </label>

      <label>
        Password
        <input name='password' type='password' />
      </label>

      {/*
        While the network request is in progress,
        we disable the button. You can always add
        more stuff, like loading spinners and whatnot.
      */}
      <Link to='/login'>Login</Link>
    </form>
  );
}
