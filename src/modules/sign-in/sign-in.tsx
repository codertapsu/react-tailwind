import React, { FormEvent } from 'react';
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useAuthenticationService } from '@security/authentication.service';
import { useRouter } from '@hook/use-router';

// Again, uncontrolled forms!
const SignIn = () => {
  const router = useRouter();
  const { login } = useAuthenticationService();
  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    login(formData.get('email') as string, formData.get('password') as string)
      .then(user => {
        router.push('/');
        console.log(user);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <div
      className='bg-cover border-t-2 border-blue-600 h-full'
      style={{ backgroundImage: 'url("https://ik.imagekit.io/q5edmtudmz/peter-lloyd-680526-unsplash_TYZn4kayG.jpg")' }}
    >
      <div className='content px-8 py-2'>
        <div className='body mt-20 mx-8'>
          <div className='md:flex items-center justify-between'>
            <div className='w-full md:w-1/2 mr-auto' style={{ textShadow: '0 20px 50px hsla(0,0%,0%,8)' }}>
              <h1 className='text-4xl font-bold text-white tracking-wide'>Brand</h1>
              <h2 className=' text-2xl font-bold text-white tracking-wide'>
                Welcome <span className='text-gray-800'> Aboard</span>
              </h2>
              <p className='text-gray-300'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
            </div>
            <div className='w-full md:max-w-md mt-6'>
              <div className='max-w-md w-full space-y-8 p-10 bg-white rounded-xl z-10 mx-auto'>
                <div className='text-center'>
                  <h2 className='mt-6 text-3xl font-bold text-gray-900'>Welcom Back!</h2>
                  <p className='mt-2 text-sm text-gray-600'>Please sign in to your account</p>
                </div>
                <div className='flex flex-row justify-center items-center space-x-3'>
                  <span className='w-11 h-11 items-center justify-center inline-flex rounded-full font-bold text-lg  text-white  bg-blue-900 hover:shadow-lg cursor-pointer transition ease-in duration-300'>
                    <FontAwesomeIcon icon={['fab', 'facebook']} />
                  </span>
                  <span className='w-11 h-11 items-center justify-center inline-flex rounded-full font-bold text-lg  text-white bg-blue-400 hover:shadow-lg cursor-pointer transition ease-in duration-300'>
                    <FontAwesomeIcon icon={['fab', 'facebook']} />
                  </span>
                  <span className='w-11 h-11 items-center justify-center inline-flex rounded-full font-bold text-lg  text-white bg-blue-500 hover:shadow-lg cursor-pointer transition ease-in duration-300'>
                    <FontAwesomeIcon icon={['fab', 'facebook']} />
                  </span>
                </div>
                <div className='flex items-center justify-center space-x-2'>
                  <span className='h-px w-16 bg-gray-300' />
                  <span className='text-gray-500 font-normal'>OR</span>
                  <span className='h-px w-16 bg-gray-300' />
                </div>
                <form className='mt-8 space-y-6' onSubmit={handleSubmit}>
                  <input type='hidden' name='remember' defaultValue='true' />
                  <div className='relative'>
                    <div className='absolute right-0 mt-4'>
                      <svg
                        xmlns='http://www.w3.org/2000/svg'
                        className='h-6 w-6 text-green-500'
                        fill='none'
                        viewBox='0 0 24 24'
                        stroke='currentColor'
                      >
                        <path
                          strokeLinecap='round'
                          strokeLinejoin='round'
                          strokeWidth={2}
                          d='M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z'
                        />
                      </svg>
                    </div>
                    <label className='text-sm font-bold text-gray-700 tracking-wide'>Email</label>
                    <input
                      className=' w-full text-base py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500'
                      type='text'
                      name='email'
                      placeholder='mail@gmail.com'
                    />
                  </div>
                  <div className='mt-8 content-center'>
                    <label className='text-sm font-bold text-gray-700 tracking-wide'>Password</label>
                    <input
                      className='w-full content-center text-base py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500'
                      type='password'
                      name='password'
                      placeholder='Enter your password'
                    />
                  </div>
                  <div className='flex items-center justify-between'>
                    <div className='flex items-center'>
                      <input
                        id='remember'
                        type='checkbox'
                        name='remember'
                        className='h-4 w-4 bg-indigo-500 focus:ring-indigo-400 border-gray-300 rounded'
                      />
                      <label htmlFor='remember' className='ml-2 block text-sm text-gray-900'>
                        Remember me
                      </label>
                    </div>
                    <div className='text-sm'>
                      <Link to='forgot-password' className='font-medium text-indigo-500 hover:text-indigo-500'>
                        Forgot your password?
                      </Link>
                    </div>
                  </div>
                  <div>
                    <button
                      type='submit'
                      className='w-full flex justify-center bg-indigo-500 text-gray-100 p-4  rounded-full tracking-wide
                          font-semibold  focus:outline-none focus:shadow-outline hover:bg-indigo-600 shadow-lg cursor-pointer transition ease-in duration-300'
                    >
                      Sign in
                    </button>
                  </div>
                  <p className='flex flex-col items-center justify-center mt-10 text-center text-md text-gray-500'>
                    <span>Don't have an account?</span>
                    <Link
                      to='sign-up'
                      className='text-indigo-500 hover:text-indigo-500no-underline hover:underline cursor-pointer transition ease-in duration-300'
                    >
                      Sign up
                    </Link>
                  </p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
