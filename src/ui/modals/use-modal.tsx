import React, { forwardRef, Ref, useCallback, useEffect, useImperativeHandle, useState } from 'react';
import { createPortal } from 'react-dom';

import style from './style.module.scss';

const modalElement = document.getElementById('modal-root');

const Modal = ({defaultOpened, fade, children}: any, ref: Ref<unknown>): JSX.Element => {
  const [isOpen, setIsOpen] = useState(defaultOpened);

  const close = useCallback(() => setIsOpen(false), []);

  useImperativeHandle(
    ref as Ref<unknown>,
    () => ({
      open: () => setIsOpen(true),
      close,
    }),
    [close],
  );

  const handleEscape = useCallback(
    event => {
      if (event.keyCode === 27) {
        close();
      }
    },
    [close],
  );

  useEffect(() => {
    if (isOpen) {
      document.addEventListener('keydown', handleEscape, false);
    }

    return () => {
      document.removeEventListener('keydown', handleEscape, false);
    };
  }, [handleEscape, isOpen]);

  return createPortal(
    isOpen ? (
      <div className={`${style.modal} ${fade ? style.modalFade : ''}`}>
        <div className={style.modalOverlay} onClick={close} />
        <span role='button' className={style.modalClose} aria-label='close' onClick={close}>
          x
        </span>
        <div className={style.modalBody}>{children}</div>
      </div>
    ) : null,
    modalElement,
  );
};

export default forwardRef(Modal);
