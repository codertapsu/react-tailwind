import { createContext, useContext, useReducer } from 'react';

import { generateUUID } from '../../helpers/generate-uuid';
import { ToastAction } from './models/toast-action.model';
import { ToastState } from './models/toast-state.model';
import { Toast } from './models/toast.model';
import { ToastContainer } from './toast-container/toast-container';

const maxDisplayedItems = 8;

const ToastContext = createContext<ToastState>(undefined);

const useToast = () => {
  const context = useContext(ToastContext);
  if (context === undefined) {
    throw new Error('useToast must be used within a ToastProvider');
  }

  const addToast = (toast: Toast) => context.addToast(toast);
  const removeToast = (id: string) => context.removeToast(id);

  return { addToast, removeToast, items: context.items };
};

const toastReducer = (state: ToastState, action: ToastAction) => {
  switch (action.type) {
    case 'ADD_TOAST': {
      const items = state.items;
      if (items.length > maxDisplayedItems - 1) {
        items.splice(0, 1);
      }
      items.push(action.payload);

      return { ...state, items };
    }
    case 'REMOVE_TOAST':
      return {
        ...state,
        items: state.items.filter(it => it.id !== action.payload.id),
      };
    default: {
      return state;
    }
  }
};

const ToastProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(toastReducer, { items: [] });

  const addToast = (toast: Toast) => {
    const id = generateUUID();
    dispatch({
      type: 'ADD_TOAST',
      payload: { ...toast, id },
    });

    return id;
  };

  const removeToast = (id: string) =>
    dispatch({
      type: 'REMOVE_TOAST',
      payload: {
        id,
        type: 'success',
        message: '',
      },
    });

  const value = {
    addToast,
    removeToast,
    items: state.items,
  };

  return (
    <ToastContext.Provider value={value}>
      {children}
      <ToastContainer />
    </ToastContext.Provider>
  );
};

export { ToastProvider, useToast };
