import { useRef } from 'react';

import { Toast } from '../models/toast.model';
import ToastItemStyle from './toast-item.module.scss';

const ToastItem = ({ remove, toast }: { remove: (child: HTMLDivElement) => void; toast: Toast }) => {
  const ref = useRef<HTMLDivElement>(undefined);

  return (
    <div ref={ref} className={ToastItemStyle.wrapper}>
      <div className={`${ToastItemStyle.toast} ${ToastItemStyle.offline}`}>
        <div className={ToastItemStyle.content}>
          <div className={ToastItemStyle.icon}>
            <i className='uil uil-wifi' />
          </div>
          <div className={ToastItemStyle.details}>
            <span>You're online now</span>
            <p>{toast.id}</p>
          </div>
        </div>
        <div className={ToastItemStyle.closeIcon} onClick={() => remove(ref.current)}>
          <i className='uil uil-times' />
        </div>
      </div>
    </div>
  );
};

export { ToastItem };
