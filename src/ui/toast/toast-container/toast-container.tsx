import { useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';

import { useDocument } from '../../../hook/use-document';
import { ToastItem } from '../toast-item/toast-item';
import { useToast } from '../toast.context';

const useComponentWillMount = (callback: () => void) => {
  const willMount = useRef(true);
  if (willMount.current) {
    callback();
  }
  useEffect(() => {
    willMount.current = false;
  }, []);
};

const ToastContainer = () => {
  const { items, removeToast } = useToast();
  const [instanceDocument] = useDocument();
  const [toastPortal, setToastPortal] = useState<HTMLElement>(instanceDocument.body.querySelector<HTMLElement>(':scope > #toast-root'));
  useComponentWillMount(() => {
    if (!toastPortal) {
      const portal = instanceDocument.createElement('div');
      portal.setAttribute('id', 'toast-root');
      instanceDocument.body.appendChild(portal);
      setToastPortal(portal);
    }
  });

  const remove = (toastItem: HTMLDivElement, id: string) => {
    const animation = toastItem.animate([{ height: `${toastItem.clientHeight}px` }, { height: 0 }], {
      duration: 300,
      fill: 'forwards',
    });
    animation.onfinish = () => {
      console.log('Finish');
      removeToast(id);
    };
  };

  return !toastPortal
    ? null
    : createPortal(
        <>
          {(items || []).map(it => (
            <ToastItem key={it.id} toast={it} remove={child => remove(child, it.id)} />
          ))}
        </>,
        toastPortal,
      );
};

export { ToastContainer };
