import { ToastPosition } from './toast-position.model';

export interface ToastConfig {
  duration: number;
  position: ToastPosition;
  customIcon?: string;
}
