import { Toast } from './toast.model';

export interface ToastAction {
  type: 'ADD_TOAST' | 'REMOVE_TOAST';
  payload?: Toast;
}
