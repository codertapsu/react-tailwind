export type ToastPosition = 'default' | 'topRight' | 'topLeft' | 'bottomRight' | 'bottomLeft';
