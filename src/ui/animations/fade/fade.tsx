import React, { memo, useCallback, useEffect, useRef, useState } from 'react';

import fadeStyle from './fade.module.scss';

const Fade = memo(({ show, children }: any) => {
  const [render, setRender] = useState(show);

  useEffect(() => {
    if (show) {
      setRender(true);
    }
  }, [show]);

  const onAnimationEnd = () => {
    if (!show) {
      setRender(false);
    }
  };

  return (
    render && (
      <div
        style={{
          animation: `${show ? fadeStyle.bounceIn : fadeStyle.bounceOut} 600ms`,
          position: 'relative',
        }}
        onAnimationEnd={onAnimationEnd}
      >
        {children}
      </div>
    )
  );
});

export default Fade;
