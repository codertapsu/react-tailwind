const path = require(`path`);

module.exports = {
  webpack: {
    alias: {
      '@contexts': path.resolve(__dirname, 'src/contexts'),
      '@hook': path.resolve(__dirname, 'src/hook'),
      '@models': path.resolve(__dirname, 'src/models'),
      '@security': path.resolve(__dirname, 'src/security'),
      '@services': path.resolve(__dirname, 'src/services'),
      '@ui': path.resolve(__dirname, 'src/ui'),
    },
  },
};
